const express = require('express');
const bodyParser = require('body-parser');
const url = require('url');
const cors = require('cors');
const http = require('http');
const { router, getWebSocketServer } = require('./routes/routes');

const server = http.createServer();
const app = express();
const port = process.env.PORT;

server.on('upgrade', async (request, socket, head) => {
  const instanceId = url.parse(request.url).pathname.substr(1);
  const webSocketServer = await getWebSocketServer(instanceId);

  webSocketServer.wss.handleUpgrade(request, socket, head, (ws) => {
    webSocketServer.wss.emit('connection', ws, request);
  });
});

server.listen(8000);

app.use(cors());

app.use(bodyParser.json());

app.use('/api', router);

app.listen(port, () => console.log(`listening on port ${port}!`));
