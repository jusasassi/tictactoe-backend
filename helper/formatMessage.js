const formatMessage = (messageType, data) => {
  const message = {
    type: messageType,
    data,
  };
  return JSON.stringify(message);
};

export default { formatMessage };
