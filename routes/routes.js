const express = require('express');
const TicTacToe = require('./../business/ticTacToe');

const router = express.Router();
const ticTacToe = new TicTacToe();

const checkLogin = async (req, res) => {
  try {
    const status = await ticTacToe.CheckLogin(req.query);
    res.status(200).send(status);
  } catch (err) {
    res.status(500).send({ message: err.toString() });
  }
};

const createLogin = async (req, res) => {
  try {
    const login = await ticTacToe.CreateLogin(req.query);
    res.status(200).send(login);
  } catch (err) {
    res.status(500).send({ message: err.toString() });
  }
};

const clearLogin = async (req, res) => {
  try {
    const clearStatus = await ticTacToe.ClearLogin(req.query);
    res.status(200).send(clearStatus);
  } catch (err) {
    res.status(500).send({ message: err.toString() });
  }
};

const getWebSocketServer = async (instanceId) => {
  try {
    const webSocketServer = await ticTacToe.GetWebSocketServer(instanceId);
    return webSocketServer;
  } catch (err) {
    return err.toString();
  }
};

router.get('/checkLogin', checkLogin);
router.get('/createLogin', createLogin);
router.get('/clearLogin', clearLogin);

module.exports = { router, getWebSocketServer };
