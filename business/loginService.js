const { guid } = require('../components/guid').default;

function LoginService() {
  this.activeLogins = [];

  const checkTimeOut = () => {
    const currentTime = Date.now();
    let login;
    for (let i = this.activeLogins.length - 1; i >= 0; i -= 1) {
      login = this.activeLogins[i];
      if (login.activeTill < currentTime) {
        console.log(`cleared login ${login.loginId}, user ${login.userName}`);
        this.activeLogins.splice(i, 1);
      }
    }
  };

  setInterval(checkTimeOut, 10000);
}

LoginService.prototype.CreateLogin = async function (userName) {
  this.activeLogins.forEach((login) => {
    if (login.userName === userName) throw new Error('NAME ALREADY TAKEN');
  });
  const loginId = guid();
  const activeTill = new Date(600000 + Date.now());
  const login = {
    loginId,
    userName,
    activeTill,
  };
  this.activeLogins.push(login);
  return login;
};

LoginService.prototype.CheckLogin = async function (loginId) {
  let status = false;
  this.activeLogins.forEach((login) => {
    if (login.loginId === loginId) {
      status = true;
    }
  });
  return status;
};

LoginService.prototype.GetLogin = async function (loginId) {
  let result = false;
  this.activeLogins.forEach((login) => {
    if (login.loginId === loginId) {
      result = login;
    }
  });
  console.log(result);
  return result;
};

LoginService.prototype.ClearLogin = async function (loginId) {
  const startLength = this.activeLogins.length;
  this.activeLogins = this.activeLogins.filter((login) => login.loginId !== loginId);
  if (startLength !== this.activeLogins.length) return true;
  return false;
};

module.exports = LoginService;
