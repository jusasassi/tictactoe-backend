const Lobby = require('../components/lobby');
const Room = require('../components/room');

function RoomService(loginService) {
  this.gameSessions = [];
  this.loginService = loginService;
  this.rooms = [];
  this.lobby = new Lobby(this);
}

RoomService.prototype.RefreshRoomsAllClients = async function () {
  this.lobby.RefreshRoomsAllClients();
};

RoomService.prototype.CreateRoom = async function (data) {
  const gameSession = data;
  const room = new Room(gameSession, this);
  this.rooms.push(room);
  return room.id;
};

RoomService.prototype.GetRooms = function () {
  const rooms = this.rooms.map((webSocketServer) => webSocketServer.sessionData);
  return rooms;
};

RoomService.prototype.RemoveRoom = function (id) {
  for (let i = this.rooms.length - 1; i >= 0; i -= 1) {
    if (this.rooms[i].id === id) {
      this.rooms.splice(i, 1);
      console.log(`Removed room ${id}`);
    }
  }
};

RoomService.prototype.GetLogin = async function (loginId) {
  return this.loginService.GetLogin(loginId);
};

RoomService.prototype.GetWebSocketServer = async function (sessionId) {
  let webSocketServer = null;
  if (sessionId === 'lobby') return this.lobby;
  this.rooms.forEach((socketServer) => {
    if (socketServer.id.toString() === sessionId) webSocketServer = socketServer;
  });
  return webSocketServer;
};

module.exports = RoomService;
