const LoginService = require('./loginService');
const RoomService = require('./roomService');
const { REQUEST_TYPE } = require('../helper/messageTypes');

function TicTacToe() {
  this.loginService = new LoginService();
  this.roomService = new RoomService(this.loginService);
}

TicTacToe.prototype.CheckLogin = async function ({ loginId }) {
  return this.loginService.CheckLogin(loginId);
};

TicTacToe.prototype.CreateLogin = async function ({ userName }) {
  const login = await this.loginService.CreateLogin(userName);
  return login;
};

TicTacToe.prototype.ClearLogin = async function ({ loginId }) {
  return this.loginService.ClearLogin(loginId);
};

TicTacToe.prototype.GetLoginInfo = async function (loginId) {
  return this.loginService.GetLoginInfo(loginId);
};

TicTacToe.prototype.ParseWebSocketMesssage = async function ({ loginId, message }) {
  let result;
  if (this.loginService.CheckLogin(loginId)) {
    if (message.type === REQUEST_TYPE.CREATE_ROOM) {
      result = await this.roomService.ParseMessage(loginId, message);
    }
  }
  return result;
};

TicTacToe.prototype.GetWebSocketServer = async function (instanceId) {
  const result = this.roomService.GetWebSocketServer(instanceId);
  return result;
};

module.exports = TicTacToe;
