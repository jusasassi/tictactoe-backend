const WebSocket = require('ws');
const { guid } = require('./guid').default;
const { formatMessage } = require('../helper/formatMessage').default;
const { MESSAGE_TYPE } = require('../helper/messageTypes');

class Room {
  constructor(initData, roomService) {
    this.roomService = roomService;
    this.wss = new WebSocket.Server({ noServer: true });
    this.clients = [];
    this.id = guid();
    this.sessionData = {
      name: initData.name,
      id: this.id,
      host: initData.host,
    };
    console.log(`webSocketServer created at ${this.id}`);

    this.wss.on('connection', (webSocket, request) => {
      const socketId = request.headers['sec-websocket-key'];
      this.clients.push({ webSocket, socketId });
      const client = this.clients[this.connectionIndexBySocketId(socketId)];
      console.log(`new connection at ${this.id}`);

      webSocket.on('message', async (message) => {
        console.log(`webSocketServer message received at ${this.id}`);
        const parsedMessage = JSON.parse(message);
        const messageType = parsedMessage.type;
        const { data } = parsedMessage;

        console.log(parsedMessage);
        switch (messageType) {
          case MESSAGE_TYPE.MESSAGE_ROOM:
            await this.MessageRoom(data.message, client);
            break;
          case MESSAGE_TYPE.REMOVE_ROOM:
            if (client.loginId === this.sessionData.host) this.RemoveRoom();
            break;
          case MESSAGE_TYPE.CONNECT_ROOM: {
            client.loginId = data.loginId;
            const login = await this.roomService.GetLogin(data.loginId);
            client.name = login.userName;
            this.AnnounceConnectionStatus(client, 'connected');
            break; }
          default:
            console.log('Unhandled message type');
        }
      });

      webSocket.on('close', () => {
        this.HandleSocketClosure(socketId, client);
      });
    });
  }

  HandleSocketClosure(socketId, client) {
    console.log('connection closed');
    this.AnnounceConnectionStatus(this.clients[this.connectionIndexBySocketId(socketId)], 'disconnected');
    const hostId = client.loginId;
    this.clients.splice(this.connectionIndexBySocketId(socketId), 1);
    if (this.clients.length > 0) {
      if (hostId === this.sessionData.host) {
        this.TransferHostStatus(0);
      }
    } else {
      this.RemoveRoom();
    }
  }

  TransferHostStatus(newHostId) {
    const index = this.connectionIndexByLoginId(newHostId);
    this.sessionData.host = this.clients[index].loginId;
    const sendMessage = formatMessage(
      MESSAGE_TYPE.MESSAGE_ROOM,
      { message: `Host privileges transferred to ${this.clients[index].name}` },
    );
    this.clients.forEach((client) => {
      client.webSocket.send(sendMessage);
    });
  }

  async MessageRoom(receivedMessage, sentClient) {
    const message = `${sentClient.name}: ${receivedMessage}`;
    const sendMessage = formatMessage(
      MESSAGE_TYPE.MESSAGE_ROOM,
      { message },
    );
    this.clients.forEach((client) => {
      client.webSocket.send(sendMessage);
    });
  }

  async AnnounceConnectionStatus(newClient, message) {
    const sendMessage = formatMessage(
      MESSAGE_TYPE.MESSAGE_ROOM,
      { message: `${newClient.name} ${message}` },
    );
    this.clients.forEach((client) => {
      client.webSocket.send(sendMessage);
    });
  }

  RemoveRoom() {
    this.roomService.RemoveRoom(this.id);
    this.roomService.RefreshRoomsAllClients();
    const sendMessage = formatMessage(MESSAGE_TYPE.DISCONNECT);
    this.clients.forEach((client) => {
      client.webSocket.send(sendMessage);
    });
    this.wss.close();
  }

  connectionIndexBySocketId(id) {
    return this.clients.findIndex((client) => client.socketId === id);
  }

  connectionIndexByLoginId(id) {
    return this.clients.findIndex((client) => client.loginId === id);
  }
}

module.exports = Room;
