const WebSocket = require('ws');
const { formatMessage } = require('../helper/formatMessage').default;
const { MESSAGE_TYPE } = require('../helper/messageTypes');

class Lobby {
  constructor(roomService) {
    this.wss = new WebSocket.Server({ noServer: true });
    this.roomService = roomService;
    this.clients = [];
    console.log('Lobby creted');

    this.wss.on('connection', (webSocket, request) => {
      const socketId = request.headers['sec-websocket-key'];
      this.clients.push({ webSocket, socketId });
      console.log('new connection in lobby');
      this.RefreshRoomsOnConnect(webSocket);

      webSocket.on('message', async (message) => {
        console.log('Lobby reveived message');
        console.log(message);
        const parsedMessage = JSON.parse(message);
        const messageType = parsedMessage.type;
        const { data } = parsedMessage;
        switch (messageType) {
          case MESSAGE_TYPE.CREATE_ROOM: {
            const roomId = await this.CreateRoom(data);
            console.log(roomId);
            webSocket.send(formatMessage(MESSAGE_TYPE.CONNECT_ROOM, { roomId }));
            break; }
          default:
            console.log('Unhandled message');
        }
      });

      webSocket.on('close', () => {
        console.log('connection closed');
        this.clients.splice(this.connectionIndex(socketId), 1);
      });
    });
  }

  async CreateRoom(data) {
    const roomId = await this.roomService.CreateRoom(data);
    console.log('room created');
    this.RefreshRoomsAllClients();
    return roomId;
  }

  RefreshRoomsOnConnect(webSocket) {
    const sendMessage = formatMessage(
      MESSAGE_TYPE.CREATE_ROOM,
      { rooms: this.roomService.GetRooms() },
    );
    webSocket.send(sendMessage);
  }

  RefreshRoomsAllClients() {
    this.clients.forEach((client) => {
      const sendMessage = formatMessage(
        MESSAGE_TYPE.CREATE_ROOM,
        { rooms: this.roomService.GetRooms() },
      );
      client.webSocket.send(sendMessage);
    });
  }

  connectionIndex(id) {
    return this.clients.findIndex((client) => client.socketId === id);
  }
}

module.exports = Lobby;
